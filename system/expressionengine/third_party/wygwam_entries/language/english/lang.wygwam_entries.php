<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['preference'] = 'Preference';
$lang['setting'] = 'Setting';
$lang['uri_var_instructions'] = 'Variables available in uri\'s';
$lang['submit'] = 'Submit';
$lang['key'] = 'License Key';
$lang['uri_auto_instructions'] = 'You may also use just {auto_path} or {id_auto_path} to use the path from channel settings.';
