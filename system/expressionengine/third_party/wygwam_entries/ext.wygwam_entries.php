<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @package   Wygwam_Entries
 * @author    	Toby Evans (@t0bz)
 * @copyright Copyright (c) 2011, Toby Evans.
 * @license   http://devot-ee.com/add-ons/license/wygwam-entries
 * @link      http://devot-ee.com/add-ons/wygwam-entries
 * @since    Version 1.0
 *
 */

class Wygwam_entries_ext {

	var $name 		= 'Wygwam Entries';
	var $version        	= '1.2.1';
	var $description    	= 'Replaces(by default) the Site Page dropdown option in wygwam\'s link window, allowing for any channel entry - not just Pages Module entries.';
	var $settings_exist 	= 'y';
	var $docs_url       	= '';

	var $settings = array(
		'channels'=>array()
	);

	/**
	 * Constructor
	 *
	 * @param   mixed   Settings array or empty string if none exist.
	 */
	function __construct($settings='')
	{
		$this->EE =& get_instance();
		$this->EE->load->helper('array');
		$this->current_site = $this->EE->config->item('site_id');

		if (isset($settings[$this->current_site])) {
			$this->settings = $settings[$this->current_site];
		} else {
			$this->settings = $settings;
		}
	}

	function add_entries($config, $settings)
	{
		// If another extension shares the same hook,
		// we need to get the latest and greatest config
		if ($this->EE->extensions->last_call !== FALSE)
		{
			$config = $this->EE->extensions->last_call;
		}

		if (element('channels', $this->settings)) {

			$entries = $this->EE->db
				->select('exp_channel_titles.entry_id, exp_channel_titles.title, exp_channel_titles.url_title, exp_channel_titles.channel_id, t.channel_title, t.channel_name, t.search_results_url, t.channel_url, s.site_id, s.site_label')
				->join('channels t', 't.channel_id = exp_channel_titles.channel_id')
				->join('sites s', 's.site_id = t.site_id')
				->where_in('t.channel_id', element('channels', $this->settings))
				->order_by('channel_title, title')
				->get('channel_titles exp_channel_titles');

			$entry_array = array();
			$site_pages = config_item('site_pages');
			$site_pages = $site_pages ? $site_pages : array();
			$site_url = config_item('site_url');
			$page_uris = array();
			foreach ($site_pages as $site_id => $pages) {
				// only show page uri's for this current site.
				if ($site_id == $this->current_site) {
					$page_uris[$site_id] = $pages['uris'];
					break;
				}
			}

			foreach ($entries->result() as $entry) {

				// set uri variables
				$replace = array('{url_title}', '{channel_name}', '{entry_id}');
				$with = array($entry->url_title, $entry->channel_name, $entry->entry_id);

				// use page uri if it exists, else use the uri defined in the settings for this channel
				$url = (count($page_uris) && isset($page_uris[$entry->site_id])) ? element($entry->entry_id, $page_uris[$entry->site_id]) : '';
				if ($url) {
					// it is a page_uri, make sure its prefixed with the site_url
					$url = '{site_url}/'.trim($url, '/');
				} else {
					// it isn't a pages_uri
					$url = trim(element('Uri:'.$entry->channel_name, $this->settings));
					if ($url == '{auto_path}' || $url == '{id_auto_path}') {

						if ($url == '{auto_path}') {
							// Use auto path
							$auto_path = ($entry->search_results_url != '') ? $entry->search_results_url : $entry->channel_url;
							$url = $this->EE->functions->remove_double_slashes($this->EE->functions->prep_query_string($auto_path).'/'.$entry->url_title);
						} else {
							// id_auto_path
							$url = $this->EE->functions->remove_double_slashes($this->EE->functions->prep_query_string($url).'/'.$entry->entry_id);
						}

						// Remove the site url if it exists in the auto_path
						$url = str_replace($site_url, '{site_url}', $url);
					} else {
						$url = '{site_url}/'.trim(str_replace($replace, $with, element('Uri:'.$entry->channel_name, $this->settings)), '/');
					}
				}

				// add the entry to the dropdown
				if ($entry->site_id != $this->current_site) {
					$entry->channel_title = $entry->site_label.': '.$entry->channel_title;
				}
				$entry_array[] = array(
					'label'=>$entry->channel_title.' / '.$entry->title,
					'url'=>$url,
					'class'=>'entry-item',
				);
			}
			$config['link_types'][element('label', $this->settings, 'Site Page')] = $entry_array;
		}

		get_instance()->cp->add_to_foot('
			<link rel="stylesheet" href="'.URL_THIRD_THEMES.'wygwam_entries/chosen/chosen.min.css">
			<script src="'.URL_THIRD_THEMES.'wygwam_entries/chosen/chosen.jquery.min.js" type="text/javascript"></script>
			<script src="'.URL_THIRD_THEMES.'wygwam_entries/wygwam.entries.min.js" type="text/javascript" data-field-id="'.element('label', $this->settings, 'Site Page').'"></script>');

		return $config;
	}


	/**
	 * =========
	 * Settings
	 * =========
	 */
	public function settings_form($current)
	{
		if (isset($current[$this->current_site])) {
			$current = $current[$this->current_site]; // load site specific settings, but failsafe to old settings
		}
		$this->EE->load->library('autoform');
		$this->EE->autoform->buttons = '<button type="submit" class="submit">'.lang('submit').'</button>';

		// load list of channels
		$this->EE->load->library('api'); $this->EE->api->instantiate('channel_structure');
		$channels = $this->EE->api_channel_structure->get_channels($this->current_site);

		// Show a friendly message if no channels ahve been created
		if ( ! $channels) return '<p>No Channels</p>';

		// format the channel array for the dropdown
		$channels_array = array();
		$channel_data_array = array();
		foreach ($channels->result() as $channel)
		{
			$channels_array[$channel->channel_id] = $channel->channel_title;
			$channel_data_array[$channel->channel_id] = array(
				$channel->channel_name, // for multi-site this would have the site name first
				$channel->channel_name,
				$channel->channel_title
			);
		}
		$channels->free_result();

		// load Sites
		$sites = $this->EE->db
				->select('exp_sites.site_id, exp_sites.site_label, exp_sites.site_name')
				->where('exp_sites.site_id !=',$this->current_site)
				->order_by('exp_sites.site_label')
				->get('sites exp_sites');

		if ($sites) { // if this is a multisite install
			foreach ($sites->result() as $site) {
				// Load site channels
				$channels = $this->EE->api_channel_structure->get_channels($site->site_id);
				
				// skip this site if there are no channels
				if (!$channels) continue;
				
				// loop over channels & create dropdown array
				foreach ($channels->result() as $channel)
				{
					$channels_array[$channel->channel_id] = $site->site_label . ':  ' . $channel->channel_title;
					$channel_data_array[$channel->channel_id] = array(
						$site->site_name.'_'.$channel->channel_name,
						$channel->channel_name,
						$site->site_label.': '.$channel->channel_title
					);
				}
				$channels->free_result();
			}
			$sites->free_result();
		}

		// license key field
		$this->EE->autoform->add(array('name'=>'key', 'value'=>element('key', $current), 'label'=>lang('key')));

		// build up the required fields
		$this->EE->autoform->add(array('name'=>'label', 'value'=>element('label', $current, 'Site Page')));
		$this->EE->autoform->add(array('name'=>'channels[]', 'type'=>'select', 'multiple'=>'multiple', 'options'=>$channels_array, 'value'=>element('channels', $current, $channels_array)));
		foreach ($channel_data_array as $channel)
		{
			$this->EE->autoform->add(array(
				'name'=>'Uri:'.$channel[0],
				'value'=>element('Uri:'.$channel[0], $current, $channel[1].'/{url_title}'
			)));
		}

		return $this->EE->load->view('settings', array('channels'=>$channel_data_array, 'current'=>$current), TRUE);
	}


	/**
	 * Save Settings
	 *
	 * This function provides a little extra processing and validation
	 * than the generic settings form.
	 *
	 * @return void
	 */
	function save_settings()
	{
		if (empty($_POST))
		{
			show_error($this->EE->lang->line('unauthorized_access'));
		}

		unset($_POST['submit']);
		$currents = $this->EE->db->where('class', __CLASS__)->limit(1)->get('extensions');
		$settings = array();
		foreach ($currents->result() as $current) {
			$settings = unserialize($current->settings);
		}
		$settings[$this->current_site] = $_POST;
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update('extensions', array('settings' => serialize($settings)));

		$this->EE->session->set_flashdata(
			'message_success',
			$this->EE->lang->line('preferences_updated')
		);
	}


	/**
	 * Activate Extension
	 *
	 * This function enters the extension into the exp_extensions table
	 *
	 * @see http://codeigniter.com/user_guide/database/index.html for
	 * more information on the db class.
	 *
	 * @return void
	 */
	function activate_extension()
	{
		$this->settings = array(
			'channels' => array('pages')
		);


		$data = array(
			'class'     => __CLASS__,
			'method'    => 'add_entries',
			'hook'      => 'wygwam_config',
			'settings'  => serialize($this->settings),
			'priority'  => 10,
			'version'   => $this->version,
			'enabled'   => 'y'
		);

		$this->EE->db->insert('extensions', $data);
	}

	/**
	 * Called by ExpressionEngine updates the extension
	 *
	 * @return void
	 **/
	public function update_extension($current=FALSE)
	{
		if($current == $this->version) return false;

		// Update the extension
		$this->EE->db->where('class', __CLASS__)->update('extensions', array('version' => $this->version));

	}

	/**
	 * Disable Extension
	 *
	 * This method removes information from the exp_extensions table
	 *
	 * @return void
	 */
	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}

}
// END CLASS
