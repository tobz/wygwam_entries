<?php echo $this->autoform->open('C=addons_extensions'.AMP.'M=save_extension_settings'.AMP.'file=wygwam_entries')?>

<table class="mainTable padTable" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th width="20%"><?php echo lang('preference')?></th>
			<th><?php echo lang('setting')?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $this->autoform->label('key')?></td>
			<td><?php echo $this->autoform->field('key', array('label'=>FALSE))?></td>
		</tr>
			<td><?php echo $this->autoform->label('label')?></td>
			<td><?php echo $this->autoform->field('label', array('label'=>FALSE))?></td>
		</tr>
		<tr>
			<td><?php echo $this->autoform->label('channels')?></td>
			<td><?php echo $this->autoform->field('channels', array('label'=>FALSE))?></td>
		</tr>
		<tr>
			<td colspan="2"><?php echo lang('uri_var_instructions')?>: {url_title}, {entry_id}, {channel_name}</td>
		</tr>
		<tr>
			<td colspan="2"><?php echo lang('uri_auto_instructions')?></td>
		</tr>
		<?php foreach ($channels as $id => $channel) { ?>
		<tr id="url:<?php echo $channel[0]?>" data-id="<?php echo $id?>">
			<td><label for="Uri:<?php echo $channel[0]?>">Uri for: <span style="font-weight: normal;"><?php echo $channel[2]?></span></label></td>
			<td><?php echo $this->autoform->field('Uri:'.$channel[0], array('label'=>FALSE))?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<script>
$('#channels').change(function(event){
	$('tr[data-id]').hide();
	$(':selected', this).each(function(){
		$('[data-id="'+this.value+'"]').show();
	});
});
$('tr[data-id]').hide();
$(':selected', '#channels').each(function(){
	$('[data-id="'+this.value+'"]').show();
});
</script>

<?php echo $this->autoform->buttons?>

<?php echo $this->autoform->close()?>